﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public int damage = 10;

    void OnCollisionEnter(Collision collision) {
        var hit = collision.gameObject;
        var hitPlayer = hit.GetComponent<PlayerMove>();
        if (hitPlayer != null) {
            var combat = hit.GetComponent<Combat>();
            combat.TakeDamage(damage);
            Destroy(gameObject);
        }
    }

}
