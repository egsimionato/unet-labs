﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Combat : NetworkBehaviour {

    public const int maxHealth = 100;
    public bool destroyOnDeath = true;

    [SyncVar]
    public int health = maxHealth;

    public void TakeDamage(int amount) {
        if (!isServer) {
            return;
        }
        health -= amount;
        if (health <= 0) {
            Destroy(gameObject);
           // if (destroyOnDeath) {
           // } else {
           //     health = maxHealth;
           //     RpcRespawn();
           // }
        }
    }

    [ClientRpc]
    void RpcRespawn() {
        if (isLocalPlayer) {
            transform.position = new Vector3(2, 0, 2);
        }
    }

}
