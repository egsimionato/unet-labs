﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerMove : NetworkBehaviour {

    public GameObject bulletPrefab;

    public override void OnStartLocalPlayer() {
        GetComponent<MeshRenderer>().material.color = Color.grey;

    }

	void Update () {
        if (!isLocalPlayer) {
            return;
        }

        var x = Input.GetAxis("Horizontal") * 0.1f;
        var z = Input.GetAxis("Vertical") * 0.1f;

        transform.Translate(x, 0, z);

        if (Input.GetKeyDown(KeyCode.Space)) {
            CmdFire();
        }
	}

    [Command] // is run on the server!
    private void CmdFire() {
        var bullet = (GameObject)Instantiate(
            bulletPrefab,
            transform.position - transform.forward,
            Quaternion.identity);
        // make the bullet move away in front of the player
        bullet.GetComponent<Rigidbody>().velocity = -transform.forward * 4;
        // spawn the bullet on the clients
        NetworkServer.Spawn(bullet);
        // make bullet disappear after 2 seconds
        // whe the bullet destroyed on the server it will automatically be destroyed on clients
        Destroy(bullet, 2.0f); 
    }
}
